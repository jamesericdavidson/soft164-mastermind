# Mastermind

Coursework for SOFT164 - Operating Systems, Data Structures and Algorithms

*A code-breaking game for two players*

![Screenshot demonstration of playing Mastermind](demo.png "A Game of Mastermind")

---

* The coding task was submitted as a single `.cs` file
    * An `.exe` compiled with `csc` is provided for demonstration purposes
* A console application was requested, not a graphical user interface
* In spite of being written in C# for .NET and Mono, constructs atop C were to
  be avoided
