﻿/*
 * Copyright 2018 James Davidson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;

namespace Mastermind
{
	public class Mastermind
	{
		public static void Main(string[] args)
		{
			Mastermind mastermind = new Mastermind();
			const int positionsUpperLimit = 100;
			const int coloursUpperLimit = 9;
			int numberOfPositions = 0;
			int numberOfColours = 0;

			Console.WriteLine("Set the parameters for this game:");
			numberOfPositions = mastermind.SetParameter (numberOfPositions, positionsUpperLimit);
			numberOfColours = mastermind.SetParameter (numberOfColours, coloursUpperLimit);

			mastermind.Play(numberOfPositions, numberOfColours);
		}

		private void Play(int numberOfPositions, int numberOfColours)
		{
			int[] pattern = GeneratePattern(numberOfPositions, numberOfColours);
			int[] guess;

			do
			{
				Console.WriteLine(string.Format("Enter guess (1 - {0}):", numberOfColours));

				guess = EnterGuess(numberOfPositions, numberOfColours);
				int[] evaluation = EvaluateGuess(guess, pattern);

				Console.WriteLine(ProvideFeedback(evaluation));
			} while (IsGuessCorrect(guess, pattern) == false);

			Console.WriteLine("You win!");
			AskUserToPlayAgain(numberOfPositions, numberOfColours);
		}

		private int SetParameter(int parameter, int upperLimit)
		{
			const int lowerLimit = 2;
			string inputError = string.Format("Input was lower than {0} or higher than {1}. " +
				"Please try again.", lowerLimit, upperLimit);
			char[] inputAsCharArray;
			bool isNumberWithinLimits = true;

			do
			{
				if (isNumberWithinLimits == false)
				{
					Console.WriteLine(inputError);
				}

				Console.WriteLine(string.Format("Number of colours ({0} - {1}):",
					lowerLimit, upperLimit));
				
				inputAsCharArray = Console.ReadLine().ToCharArray();
				parameter = ConvertCharArrayToInt(inputAsCharArray);

				isNumberWithinLimits = IsInputWithinLimit(parameter, lowerLimit, upperLimit);
			} while (isNumberWithinLimits == false);

			return parameter;
		}

		private int[] GeneratePattern(int numberOfPositions, int numberOfColours)
		{
			Random random = new Random();
			int[] pattern = new int[numberOfPositions];
			const int minValue = 1;
			int maxValue = numberOfColours + 1;

			int i;
			for (i = 0; i < numberOfPositions; i++)
			{
				pattern[i] = random.Next(minValue, maxValue);
			}

			return pattern;
		}

		private int[] EvaluateGuess(int[] guess, int[] pattern)
		{	
			int length = guess.Length;

			// comparisonPattern and comparisonGuess are copies.
			// When a value is "used" (marked as a bull or cow), it is set to
			// an unreachable value, so that it can't be used again for comparison.

			int[] comparisonPattern = new int[length];
			comparisonPattern = CopyIntArray(pattern, comparisonPattern);
			int[] comparisonGuess = new int[length];
			comparisonGuess = CopyIntArray(guess, comparisonGuess);

			// As defined in Bulls and Cows: https://en.wikipedia.org/wiki/Bulls_and_Cows#The_numerical_version
			// Where bulls are black key pegs; cows are white key pegs.

			int bulls = 0;
			int cows = 0;

			// Determine how many bulls there are.

			int i;
			for (i = 0; i < length; i++)
			{
				if (comparisonGuess[i] == comparisonPattern[i])
				{
					bulls++;
					comparisonPattern[i] = 0;
					comparisonGuess[i] = -1;
				}
			}

			// Determine how many cows there are.

			int n;
			for (i = 0; i < length; i++)
			{
				for (n = 0; n < length; n++)
				{
					if (comparisonGuess[i] == comparisonPattern[n])
					{
						cows++;
						comparisonPattern[n] = 0;
						comparisonGuess[i] = -1;
						break;
					}
				}
			}

			int[] evaluation = { bulls, cows };
			return evaluation;
		}

		private int[] CopyIntArray(int[] originalArray, int[] newArray)
		{
			int i;
			int length = originalArray.Length;

			for (i = 0; i < length; i++)
			{
				newArray[i] = originalArray[i];
			}

			return newArray;
		}

		private string ProvideFeedback(int[] evaluation)
		{
			string keyPegs = string.Format("black = {0}" + "\r\n" + "white = {1}",
				evaluation[0], evaluation[1]);
			
			return keyPegs;
		}

		private bool IsGuessCorrect(int[] guess, int[] pattern)
		{
			bool doPegsMatch = false;
			int length = guess.Length;

			int i;
			for (i = 0; i < length; i++)
			{
				if (guess[i] == pattern[i])
				{
					doPegsMatch = true;
				}
				else
				{
					doPegsMatch = false;
					break;
				}
			}

			return doPegsMatch;
		}

		private int[] EnterGuess(int numberOfPositions, int upperLimit)
		{
			int[] guess = new int[numberOfPositions];
			const int lowerLimit = 1;

			int i;
			for (i = 0; i < numberOfPositions; i++)
			{
				guess [i] = ParseGuessPeg (i, lowerLimit, upperLimit);
			}

			return guess;
		}

		private int ParseGuessPeg(int index, int lowerLimit, int upperLimit)
		{
			int guessPeg;
			string inputError = string.Format("Input was lower than {0} or higher than {1}. " +
				"Please try again.", lowerLimit, upperLimit);
			bool isInputWithinLimit = true;

			do
			{
				if (isInputWithinLimit == false)
				{
					Console.WriteLine(inputError);
				}

				string position = string.Format("Position {0}:", index + 1);
				Console.WriteLine(position);

				guessPeg = ConvertCharArrayToInt(Console.ReadLine().ToCharArray());
				isInputWithinLimit = IsInputWithinLimit(guessPeg, lowerLimit, upperLimit);
			} while (isInputWithinLimit == false);

			return guessPeg;
		}

		private void AskUserToPlayAgain(int numberOfPositions, int numberOfColours)
		{
			// Inspired by: https://rosettacode.org/wiki/Keyboard_input/Obtain_a_Y_or_N_response#C.23

			string input;

			do
			{
				Console.WriteLine("Would you like to play again? [Y / N]:");
				input = Console.ReadLine();

				if (input == "y" || input == "Y")
				{
					Play(numberOfPositions, numberOfColours);
				}
				else if (input == "n" || input == "y")
				{
					// As return does not work outside of the Main() method,
					// Environment.Exit() is called instead.
					Environment.Exit(0);
				}
			} while (input == "y" || input == "Y" || input == "n" || input == "y");
		}

		private int ConvertCharArrayToInt(char[] number)
		{
			// Inspired by: https://blog.udemy.com/c-string-to-int/

			// Converts each Unicode character in the array to a decimal number,
			// adds them, and returns them as an integer.

			int result = 0;
			int length = number.Length;

			int i;
			for (i = 0; i < length; i++)
			{
				result = (result * 10 + (number[i] - '0'));
			}

			return result;
		}

		private bool IsInputWithinLimit(int input, int lowerLimit, int upperLimit)
		{
			if (input >= lowerLimit && input <= upperLimit)
			{
				return true;
			}
			return false;
		}
	}
}
